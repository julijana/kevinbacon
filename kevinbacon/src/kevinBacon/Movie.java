/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kevinBacon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author julijana
 */
public class Movie {
    
    private String name;
    private List<Actor> actors;
    
    public Movie(String name){
        this.name = name;
        actors = new ArrayList<Actor>();
    }
    
    public void addActor(Actor actor){
        actors.add(actor);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }
    
    
    
}