/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kevinBacon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author julijana
 */
public class Actor {
    
    private String name;
    private List<Movie> movies;
    private Actor previousNode;
    private Movie sharedMovie;
    private int number;
    
    public Actor(String name){
        this.name = name;
        movies = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void add(Movie movie) {
        movies.add(movie);
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public Actor getPreviousNode() {
        return previousNode;
    }

    public void setPreviousNode(Actor previousNode) {
        this.previousNode = previousNode;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Movie getSharedMovie() {
        return sharedMovie;
    }

    public void setSharedMovie(Movie sharedMovie) {
        this.sharedMovie = sharedMovie;
    }
    
    
    
     public Map<Actor,Movie> coStarsMap(){
        Map<Actor,Movie> coStars = new HashMap<Actor,Movie>();
        for(Movie movie : this.movies){
            for(Actor actor : movie.getActors()){
                coStars.put(actor, movie);
            }
        }
        return coStars;
    }
    
}