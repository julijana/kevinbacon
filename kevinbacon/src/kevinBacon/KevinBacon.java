/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kevinBacon;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;

/**
 *
 * @author julijana
 */
public class KevinBacon {

    private HashMap<String, Actor> actors;
    private HashMap<String, Movie> movies;

    public KevinBacon() {
        actors = new HashMap<String, Actor>();
        movies = new HashMap<String, Movie>();
    }

    public void getBFS()
    {
        Actor start = actors.get("Bacon, Kevin");
        start.setNumber(0);
        start.setPreviousNode(null);

        Queue<Actor> fringe = new LinkedList<Actor>();
        fringe.add(start);
        HashSet<Actor> visited = new HashSet<Actor>();
        List<Actor> actors = new LinkedList<Actor>();
        while (!fringe.isEmpty()) {
            Actor curr = fringe.poll();
            visited.add(curr);
            Map<Actor, Movie> coStars = curr.coStarsMap();
            for (Actor a : coStars.keySet()) {
                if (!visited.contains(a)) {
                    if (!actors.contains(a)) {
                        actors.add(a);
                        fringe.add(a);
                        a.setNumber(curr.getNumber() + 1);
                        a.setPreviousNode(curr);
                        a.setSharedMovie(coStars.get(a));
                    }
                }
            }

        }

    }

    public void readFile(Scanner in) {
        while (in.hasNext()) {
            String line = in.nextLine();
            String[] elems = line.split("/");
            if (elems.length == 0) {
                System.out.println("BAD line in input!");
                continue;
            }
            Movie moov = new Movie(elems[0]);
            movies.put(elems[0], moov);
            for (int k = 1; k < elems.length; k++) {
                Actor person = actors.get(elems[k]);
                if (person == null) { 
                    person = new Actor(elems[k]);
                    actors.put(elems[k], person);
                }
                person.add(moov);
                moov.addActor(person);

            }

        }
    }

    public void printChain(String name) {
        Actor start = actors.get("Bacon, Kevin");
        Actor dest = actors.get(name);
        if (dest.getNumber() == Integer.MAX_VALUE) {
            System.out.println(dest.getName() + "has a Bacon number of infinity");
            return;
        }
        System.out.println(dest.getName() + " has a Bacon number of "
                + dest.getNumber());

        while (dest != start) {
            System.out.println(dest.getName() + " was in "
                    + dest.getSharedMovie().getName() + " with "
                    + dest.getPreviousNode().getName());
            dest = dest.getPreviousNode();
        }

    }

    public static void main(String[] args) {

        System.out.println("Enter movie data filename: ");
        Scanner scan = new Scanner(System.in);
        Scanner f = null;
        String fileName = scan.nextLine();
        KevinBacon bfs = new KevinBacon();
        String actorName = null;
        try {
          f = new Scanner(new File(fileName));
        } catch (FileNotFoundException e) {
            System.out.println("File not found " + fileName);
            e.printStackTrace();
        }
        System.out.println("Enter first name of actor: ");
        String firstName = scan.nextLine();
        if (firstName == null) {
            System.out.println("You did not enter a first name. Please try again.");
          
        }
        System.out.println("Enter last name:of actor ");
        String lastName = scan.nextLine();
        if (lastName == null) {
            System.out.println("You did not enter a last name. Please try again.");
           
        }

        actorName = lastName + ", " + firstName;
       
        bfs.readFile(f);
        bfs.getBFS();
        bfs.printChain(actorName);
    }

}